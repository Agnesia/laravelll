<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class PertanyaanController extends Controller
{
    function create() {
		return view('create') ;
	}
	
	 function store(Request $request) {
		$request->validate(
		[
        'title' => 'required|unique:questions|max:255',
        'body' => 'required|max:255'
		]);
		
		 DB::table('questions')->insert(
		[
		'title' => $request['title'], 
		'context' => $request['body']
		]);
	
    return redirect ('/pertanyaan')->with ('success', 'Berhasil disimpan');
	}
	
	function index() {
		$pertanyaan = DB::table ('questions')->get();	
		return view('index', compact('pertanyaan')) ;
	}
	
	function show($id) {
		$pertanyaan = DB::table ('questions')->where('id', $id)->first();
		return view('show', compact('pertanyaan')) ;
	}
	
	function edit($id, Request $request) {
		$pertanyaan = DB::table ('questions')->where('id', $id)->first();
		return view('edit', compact('pertanyaan')) ;
	}
	
	function update($id, Request $request) {
		$request->validate([
        'title' => 'required|unique:questions|max:255',
        'body' => 'required|max:255'
		]);
		
		$affected = DB::table('questions')
              ->where('id', $id)
              ->update(['title' => $request->title], ['context' => $request->body]);
		return redirect('/pertanyaan')->with ('success', 'Berhasil update') ;
	}
	
	function destroy($id, Request $request) {
		DB::table('questions')->where('id', '=', $id)->delete();
		
		return redirect('/pertanyaan')->with ('success', 'Berhasil dihapus') ;
	}
}
