<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    function create() {
		return view('create') ;
	}
	
	 function store(Request $request) {
		// dd($request['title']);
		 DB::table('questions')->insert(
    ['title' => $request['title'], 
    'context' => $request['body']
    ]);
	
    return redirect ('index');

	}
	
	 function index() {
		$pertanyaan = DB::table ('questions')->get();
		
		return view('index', compact('pertanyaan')) ;
	}po
	
	function show($id) {
		$pertanyaan = DB::table ('questions')->where('id', $id)->first();
		return view('create', compact('pertanyaan')) ;
	}
	
	function edit($id) {
		$pertanyaan = DB::table ('questions')->where('id', $id)->first();
		return view('edit', compact('pertanyaan')) ;
	}
	
	function update($id, Request $request) {
		$affected = DB::table('questions')
              ->where('id', $id)
              ->update(['title' => $request->title], ['context' => $request->body]);
		return redirect('/pertanyaan') ;
	}
	
	function destroy($id, Request $request) {
		DB::table('questions')->where('id', '=', $id)->delete();
		return redirect('/pertanyaan')->with ('success', 'Berhasil dihapus') ;
	}
}
