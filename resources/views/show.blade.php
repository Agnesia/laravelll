
@extends ('adminLTE.master')

@section ('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
              <form role="form" action="{{$pertanyaan->id}}" method="POST">
                @csrf
                @method ('GET')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value = "{{old ('title', $pertanyaan->title)}}" readonly="readonly" >
                  </div>
                  <div class="form-group">
                    <label for="body">Isi</label>
                    <input type="text" class="form-control" id="body" name="body"  value = "{{old ('title', $pertanyaan->context)}}" readonly="readonly">
                  </div>
                </div>
                <!-- /.card-body -->

               
              </form>
            </div>
@endsection
