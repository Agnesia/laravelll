@extends ('adminLTE.master')

@section ('content')
	<section class="content-header">
	@if (session ('success'))
		<div class="alert alert-success">{{session('success')}}</div>
	@endif
     <a href = "/pertanyaan/create" class="btn btn-primary mb-2">Tambah</a>
   <table class="table table-bordered">
		<thead>                  
			<tr>
			  <th style="width: 10px">#</th>
			  <th>Task</th>
			  <th>Progress</th>
			  <th style="width: 40px">Label</th>
			</tr>
		</thead>
		<tbody>
			@foreach($pertanyaan as $key =>$value)
			<tr>
			  <td>{{$key + 1}}</td>
			  <td>{{$value -> title}}</td>
			  <td>{{$value -> context}}</td>
				
				<td style="display: flex ">
					<a href = "/pertanyaan/{{$value->id}}" class="btn btn-info btn-sm">Show</a> 
					<a href = "/pertanyaan/{{$value->id}}/edit" class="btn btn-default btn-sm">Edit</a>
					<form action="/pertanyaan/{{$value->id}}" method="post" class="button delete-confirm">
					
						@csrf
						@method ('DELETE')
					 <button  id="btnDelete" type="submit" onclick= "return confirm('Are You Sure Want to Delete?')" class="btn btn-danger btn-sm">Delete</button>
						
						
				</td>
			</tr>
			@endforeach
			
			
	  </tbody>
</table>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
 $(".delete").on("submit", function(){
        return confirm("Are you sure?");
    });
    
    

</script>
@endsection

